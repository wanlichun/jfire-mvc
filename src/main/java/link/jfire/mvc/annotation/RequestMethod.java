package link.jfire.mvc.annotation;

public enum RequestMethod
{
    GET, POST, PUT, DELETE
}
