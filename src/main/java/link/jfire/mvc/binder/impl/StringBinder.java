package link.jfire.mvc.binder.impl;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import link.jfire.baseutil.StringUtil;
import link.jfire.mvc.binder.AbstractDataBinder;

public class StringBinder extends AbstractDataBinder
{
    
    public StringBinder(String paramName)
    {
        super(paramName);
    }
    
    @Override
    public Object binder(HttpServletRequest request, Map<String, String> map, HttpServletResponse response)
    {
        String value = map.get(paramName);
        if (StringUtil.isNotBlank(value))
        {
            return value;
        }
        else
        {
            return null;
        }
    }
}
